package com.thinkopen.ecommercestruts.action;

import java.sql.SQLException;
import java.util.List;

import org.apache.struts2.dispatcher.SessionMap;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.thinkopen.ecommercestruts.database.OrderDao;
import com.thinkopen.ecommercestruts.database.OrderDaoImp;
import com.thinkopen.ecommercestruts.model.Orders;

public class SearchOrder extends ActionSupport {
	
	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("unchecked")
	SessionMap<String, Object> session = (SessionMap<String, Object>) ActionContext.getContext().get("session");
	OrderDao orderDao = new OrderDaoImp();
	List<Orders> listOrd = null;
	int tot=0;
	
	public List<Orders> getListOrd() {
		return listOrd;
	}

	public void setListOrd(List<Orders> listOrd) {
		this.listOrd = listOrd;
	}

	public String execute(){
		int l=0;
		
		try {
			 l =(int)(session.get("rand"));
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
		
		try {
			listOrd=orderDao.searchOrder(l);
		} catch (SQLException e) {
			e.printStackTrace();
			
		}
		for(Orders o:listOrd){
			
			tot=tot+o.getTotalPrice();
		}
		
		System.out.println(l);
		System.out.println(listOrd);
		return SUCCESS;
	}

	public int getTot() {
		return tot;
	}

	public void setTot(int tot) {
		this.tot = tot;
	}

	



}

