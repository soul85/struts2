package com.thinkopen.ecommercestruts.action;

import java.sql.SQLException;

import org.apache.struts2.dispatcher.SessionMap;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.thinkopen.ecommercestruts.database.OrderDao;
import com.thinkopen.ecommercestruts.database.OrderDaoImp;
import com.thinkopen.ecommercestruts.database.ProductDao;
import com.thinkopen.ecommercestruts.database.ProductDaoImp;

public class ModifyOrder extends ActionSupport{
	
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unchecked")
	SessionMap<String, Object> session = (SessionMap<String, Object>) ActionContext.getContext().get("session");
	ProductDao productDao = new ProductDaoImp();
	OrderDao orderDao = new OrderDaoImp();
	private int productIdProd;
	private int orderNumberProd;
	
	public int getProductIdProd() {
		return productIdProd;
	}
	
	public void setProductIdProd(int productIdProd) {
		this.productIdProd = productIdProd;
	}
	
	public int getOrderNumberProd() {
		return orderNumberProd;
	}

	public void setOrderNumberProd(int orderNumberProd) {
		this.orderNumberProd = orderNumberProd;
	}

	public String execute(){
		
		try {
			productDao.increaseQuantity(this.getProductIdProd());
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		
		try {
			orderDao.deleteOrder(this.getOrderNumberProd());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
}
