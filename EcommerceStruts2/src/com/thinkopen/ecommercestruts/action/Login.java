package com.thinkopen.ecommercestruts.action;

import java.sql.SQLException;
import java.util.Map;
import java.util.Random;




import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.thinkopen.ecommercestruts.database.CustomerDao;
import com.thinkopen.ecommercestruts.database.CustomerDaoImp;
import com.thinkopen.ecommercestruts.model.Customer;

public class Login extends ActionSupport implements SessionAware{
	
	private static final long serialVersionUID = 1L;
	
	private String password;
	private String email;
	SessionMap<String,Object> sessionmap;
	private int randnumber;
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public int getRandnumber() {
		return randnumber;
	}

	public void setRandnumber(int randnumber) {
		this.randnumber = randnumber;
	}

	private Customer customer= new Customer(0,null,null,null,null,null);
	
	CustomerDao customerDao = new CustomerDaoImp();
	
	@Override
	public void setSession(Map<String, Object> map) {
		sessionmap=(SessionMap<String, Object>)map;
		
		
	}
	
	public String execute(){
		
		customer.setEmail(email);
		customer.setPassword(password);
		
		try {
			customerDao.login(customer);
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		if(customerDao.validLogin()==true){
			System.out.println(customer.getName());
			Random rand = new Random();
			randnumber = rand.nextInt((999 - 1) + 1) + 1;
			System.out.println(randnumber);
			sessionmap.put("LoginSession",customer );
			sessionmap.put("rand", this.getRandnumber());
			return SUCCESS;
		}else{
			return ERROR;
		}
			
	}

	

	

	


	
	
}
