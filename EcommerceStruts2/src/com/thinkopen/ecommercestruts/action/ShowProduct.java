package com.thinkopen.ecommercestruts.action;

import java.sql.SQLException;
import java.util.List;

import com.opensymphony.xwork2.ActionSupport;
import com.thinkopen.ecommercestruts.database.ProductDao;
import com.thinkopen.ecommercestruts.database.ProductDaoImp;
import com.thinkopen.ecommercestruts.model.Product;

public class ShowProduct extends ActionSupport {
	
	
	private static final long serialVersionUID = 1L;

	List<Product> listProd = null;
	
	ProductDao productDao = new ProductDaoImp();
	
		 public String execute(){
			 try {
				 listProd = productDao.searchProduct();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			 return SUCCESS;
		 }

		public List<Product> getListProd() {
			return listProd;
		}

		public void setListProd(List<Product> listProd) {
			this.listProd = listProd;
		}
		
	
}
