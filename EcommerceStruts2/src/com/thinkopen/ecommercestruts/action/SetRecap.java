package com.thinkopen.ecommercestruts.action;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.struts2.dispatcher.SessionMap;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.thinkopen.ecommercestruts.database.OrderDao;
import com.thinkopen.ecommercestruts.database.OrderDaoImp;
import com.thinkopen.ecommercestruts.model.Orders;

public class SetRecap extends ActionSupport{

	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("unchecked")
	SessionMap<String, Object> session = (SessionMap<String, Object>) ActionContext.getContext().get("session");
	GregorianCalendar date =new GregorianCalendar();
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy � HH:mm:ss");
	private String dateString=sdf.format(date.getTime());
	
	OrderDao orderDao = new OrderDaoImp();
	private List<Orders> list=null;
	private int tot=0;
	
	public String getDateString() {
		return dateString;
	}

	public void setDateString(String dateString) {
		this.dateString = dateString;
	}

	public List<Orders> getList() {
		return list;
	}

	public void setList(List<Orders> list) {
		this.list = list;
	}

	public int getTot() {
		return tot;
	}

	public void setTot(int tot) {
		this.tot = tot;
	}

	public String execute(){
		
		int l=0;
		
		try {
			 l =(int)session.get("rand");
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
		
		try {
			list=orderDao.searchOrder(l);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		
		for(Orders o:list){
			int b=o.getTotalPrice();
			tot=tot+b;
		}
		
		return SUCCESS;
	}
}
