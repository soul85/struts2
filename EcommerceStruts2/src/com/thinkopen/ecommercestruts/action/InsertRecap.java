package com.thinkopen.ecommercestruts.action;

import java.io.StringWriter;
import java.sql.SQLException;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.servlet.ServletContext;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.util.ServletContextAware;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.thinkopen.ecommercestruts.database.RecapDao;
import com.thinkopen.ecommercestruts.database.RecapDaoImp;
import com.thinkopen.ecommercestruts.model.Customer;
import com.thinkopen.ecommercestruts.model.MailUtility;
import com.thinkopen.ecommercestruts.model.RecapOrder;

public class InsertRecap extends ActionSupport implements ServletContextAware{

	
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unchecked")
	SessionMap<String, Object> session = (SessionMap<String, Object>) ActionContext.getContext().get("session");
	private String date;
	private int total;
	private String mail="paolopietro.oldrati@gmail.com";
	private int id=0;
	private String subject = "Order "+ this.getId() +" from Shop Online";
	private int customerId;
	private String name;
	private RecapOrder recap = new RecapOrder(0,0,0,0,null);
	private RecapDao recapDao = new RecapDaoImp();
	private String text;
	ServletContext servletContext;
	
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getId(){
		try {
			 id =(int)session.get("rand");
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
		return id;
	}
	
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext=servletContext;
		
	}
	
	
	public ServletContext getServletContext() {
		return servletContext;
	}
	public String execute(){
		
		try {
			 customerId = ((Customer) session.get("LoginSession")).getCustomerId();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
		
		recap.setOrderId(id);
		recap.setCustomerId(customerId);
		recap.setDate(date);
		recap.setPriceTotal(total);
		
		try {
			recapDao.insertRecapOrder(recap);
		} catch (SQLException e1) {
			
			e1.printStackTrace();
		}
		
		Properties props = new Properties();
		
	    props.setProperty("resource.loader", "webapp");
	    props.setProperty("webapp.resource.loader.class", "org.apache.velocity.tools.view.WebappResourceLoader");
				
        VelocityEngine ve = new VelocityEngine(props);
		

        ve.setApplicationAttribute("javax.servlet.ServletContext", this.getServletContext());
        ve.init();
        
        Template t = ve.getTemplate( "ordermail.vm" );

        VelocityContext context = new VelocityContext();
        
        try {
			 customerId = ((Customer) session.get("LoginSession")).getCustomerId();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}	
        try {
			 name = ((Customer) session.get("LoginSession")).getName();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}	
        
        
        
        context.put("name", name);
        context.put("id", this.getId());
        context.put("customerId",customerId);
        context.put("date",this.getDate());
        context.put("total",this.getTotal());
        StringWriter writer = new StringWriter();
        t.merge( context, writer );
        text = writer.toString();
        
		try {
			MailUtility.sendMail(mail,subject,text);
		} catch (MessagingException e) {
			
			e.printStackTrace();
		}
		
        return SUCCESS;
	}
	
	
	}
	





