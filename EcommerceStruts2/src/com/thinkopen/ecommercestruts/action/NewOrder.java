package com.thinkopen.ecommercestruts.action;

import java.sql.SQLException;







import org.apache.struts2.dispatcher.SessionMap;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.thinkopen.ecommercestruts.database.OrderDao;
import com.thinkopen.ecommercestruts.database.OrderDaoImp;
import com.thinkopen.ecommercestruts.database.ProductDao;
import com.thinkopen.ecommercestruts.database.ProductDaoImp;
import com.thinkopen.ecommercestruts.model.Customer;
import com.thinkopen.ecommercestruts.model.Orders;
import com.thinkopen.ecommercestruts.model.Product;

public class NewOrder extends ActionSupport {
	@SuppressWarnings("unchecked")
	SessionMap<String, Object> session = (SessionMap<String, Object>) ActionContext.getContext().get("session");
	private static final long serialVersionUID = 1L;
	private Product product = new Product(null,0,0,0,null);
	private Orders order;
	ProductDao productDao = new ProductDaoImp();
	OrderDao orderDao = new OrderDaoImp();
	private int productIdProd;
	
	
	public int getProductIdProd() {
		return productIdProd;
	}


	public void setProductIdProd(int productIdProd) {
		this.productIdProd = productIdProd;
	}


	public String execute() {
		 int l=0;
		 int m=0;
		try {
			productDao.decreaseQuantity(productIdProd);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		try {
			product=productDao.searchProductId(productIdProd);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}

		try {
			 m = ((Customer) session.get("LoginSession")).getCustomerId();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}	
		
		try {
			 l =(int)session.get("rand");
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
		
		
		
		order=new Orders(1,l,m,productIdProd,product.getName(),product.getPrice());
		
		try {
			orderDao.insertOrder(order);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		
		
		
		return SUCCESS;
	}

	

}
