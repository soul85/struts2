package com.thinkopen.ecommercestruts.model;

import javax.mail.*;
import javax.mail.internet.*;

import java.io.IOException;
import java.util.*;

public class MailUtility {
	public static void sendMail(String a, String b,String c) throws MessagingException{

//		final String username = "paolo.oldrati@thinkopen.it";
//		final String password = "5marzo1985";
//
//		Properties props = new Properties();
//		props.put("mail.smtp.auth", "true");
//		props.put("mail.smtp.starttls.enable", "true");
//		props.put("mail.smtp.host", "smtp.thinkopen.it");
//		props.put("mail.smtp.port", "587");
		
		final String PROPERTIES_FILE = "email.properties";
		Properties props = new Properties();
		try {
			props.load(MailUtility.class.getResourceAsStream(PROPERTIES_FILE));
		} catch (IOException e1) {
			
			e1.printStackTrace();
		}
		
		String username=props.getProperty("username");
		String password=props.getProperty("password");
		
		
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(a));
			message.setSubject(b);
			message.setContent(c,"text/html; charset=utf-8");

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

	}
}
