package com.thinkopen.ecommercestruts.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;



import com.thinkopen.ecommercestruts.model.RecapOrder;

public class RecapDaoImp implements RecapDao {
	
	private static final String TABLENAME="recaporder";
	List<RecapOrder> list;
	RecapOrder recap1 = new RecapOrder (0,0,0,0,null);
	List<RecapOrder> list2;
	RecapOrder recap2 = new RecapOrder (0,0,0,0,null);
	
	@Override
	public void insertRecapOrder(RecapOrder o) throws SQLException {
		
		PreparedStatement ps = null;
		
		try {
			Connection conn = ConnectionDb.getConnection();
			String createString = "insert into "+ TABLENAME+ " values (null,?,?,?,?)";
			ps=conn.prepareStatement(createString);
			ps.setInt(1,o.getOrderId());
			ps.setInt(2,o.getCustomerId());
			ps.setInt(3,o.getPriceTotal());
			ps.setString(4,o.getDate());;
			ps.executeUpdate(); 

			System.out.println("insert ok");
			
		} catch (SQLException e) {
			System.out.println("insert error");
			e.printStackTrace();
			return;
			
		}

	}

	@Override
	public List<RecapOrder> searchRecapOrder(int i) throws SQLException {
		Statement st = null;
		list=new ArrayList<RecapOrder>();
		try {
			Connection conn = ConnectionDb.getConnection();
			String createString = "SELECT * FROM " + TABLENAME+ " where orderId = "+i;

			st=conn.createStatement();
			ResultSet res =st.executeQuery(createString); 


			while(res.next()== true) {

				int a =Integer.parseInt(res.getString("orderNumber"));
				int b = Integer.parseInt(res.getString("orderId"));
				int c = Integer.parseInt(res.getString("customerId"));
				int d =Integer.parseInt(res.getString("priceTotal"));
				String e= res.getString("dateOrder");


				recap1.setOrderNumber(a);
				recap1.setOrderId(b);
				recap1.setCustomerId(c);
				recap1.setPriceTotal(d);
				recap1.setDate(e);


				list.add(new RecapOrder(recap1.getOrderNumber(),recap1.getOrderId(),recap1.getCustomerId(),recap1.getPriceTotal(),recap1.getDate()));
			}
			
			res.close();
		} catch (SQLException e) {
			System.out.println("ERROR");
			e.printStackTrace();

		}
		return list;
		
	}

	@Override
	public List<RecapOrder> searchRecapCustomerId(int i) throws SQLException {
		Statement st = null;
		list2=new ArrayList<RecapOrder>();
		try {
			Connection conn = ConnectionDb.getConnection();
			String createString = "SELECT * FROM " + TABLENAME+ " where customerId = "+i;

			st=conn.createStatement();
			ResultSet res =st.executeQuery(createString); 


			while(res.next()== true) {

				int a =Integer.parseInt(res.getString("orderNumber"));
				int b = Integer.parseInt(res.getString("orderId"));
				int c = Integer.parseInt(res.getString("customerId"));
				int d =Integer.parseInt(res.getString("priceTotal"));
				String e= res.getString("dateOrder");


				recap2.setOrderNumber(a);
				recap2.setOrderId(b);
				recap2.setCustomerId(c);
				recap2.setPriceTotal(d);
				recap2.setDate(e);


				list2.add(new RecapOrder(recap2.getOrderNumber(),recap2.getOrderId(),recap2.getCustomerId(),recap2.getPriceTotal(),recap2.getDate()));
			}
			
			res.close();
			
		} catch (SQLException e) {
			System.out.println("ERROR");
			e.printStackTrace();

		}
		return list2;
		
		
	}

}
