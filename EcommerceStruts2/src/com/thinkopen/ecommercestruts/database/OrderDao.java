package com.thinkopen.ecommercestruts.database;

import java.sql.SQLException;
import java.util.List;

import com.thinkopen.ecommercestruts.model.Orders;

public interface OrderDao {
		public void insertOrder(Orders o)throws SQLException;
		public void deleteOrder(int m) throws SQLException;
		public List<Orders> searchOrder(int l)throws SQLException;
		
}
