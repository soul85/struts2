package com.thinkopen.ecommercestruts.database;

import java.sql.SQLException;

import com.thinkopen.ecommercestruts.model.Customer;

public interface CustomerDao {
		public void insertCustomer(Customer o)throws SQLException;
		public Customer login(Customer o)throws SQLException;
		public boolean validLogin();
}
