package com.thinkopen.ecommercestruts.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.thinkopen.ecommercestruts.model.Product;

public class ProductDaoImp implements ProductDao {

	private static final String TABLENAME="products";
	List<Product> list;
	Product product1= new Product(null,0,0,0,null);
	
	public boolean executeUpdate(Connection conn, String command) throws SQLException {
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(command); 
			return true;
		} finally {

			if (stmt != null) { stmt.close(); }
		}
	}

	public Boolean executeQuery(Connection conn, String command) throws SQLException {
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			stmt.executeQuery(command); 
			return true;
		} finally {

			if (stmt != null) { stmt.close(); }
		}
	}
	@Override
	public List<Product> searchProduct() throws SQLException {
		Statement st = null;
		list=new ArrayList<Product>();
		try {
			Connection conn = ConnectionDb.getConnection();
			String createString = "SELECT * FROM " + TABLENAME;

			st=conn.createStatement();
			ResultSet res =st.executeQuery(createString); 


			while(res.next()== true) {

				String a =res.getString("name");
				System.out.println(a);
				int b = Integer.parseInt(res.getString("price"));

				System.out.println(b);

				int c = Integer.parseInt(res.getString("productId"));
				int d =Integer.parseInt(res.getString("quantity"));
				String e = res.getString("imageLink");

				product1.setName(a);
				product1.setPrice(b);
				product1.setProductId(c);
				product1.setQuantity(d);
				product1.setImageLink(e);

				list.add(new Product(product1.getName(),product1.getPrice(),product1.getProductId(),product1.getQuantity(),product1.getImageLink()));
				
			}
			res.close();
		} catch (SQLException e) {
			System.out.println("ERROR");
			e.printStackTrace();

		}
		return list;
	}
	@Override
	public void increaseQuantity(int i) throws SQLException {
		Statement st = null;
		try {
			Connection conn = ConnectionDb.getConnection();
			String createString = "update "+ TABLENAME+ " set quantity=quantity+1 where productId= "+i;

			st=conn.createStatement();
			st.executeUpdate(createString);
			System.out.println("increase ok");
			
		}catch (SQLException e) {
			
			System.out.println("increase error");
			e.printStackTrace();

		}
		
	}
		
		public void decreaseQuantity(int i) throws SQLException {
			Statement st = null;
			try {
				Connection conn = ConnectionDb.getConnection();
				String createString = "update "+ TABLENAME+ " set quantity=quantity-1 where productId= "+i;

				st=conn.createStatement();
				st.executeUpdate(createString);
				System.out.println("decrease ok");

			}catch (SQLException e) {
				System.out.println("decrease error");
				e.printStackTrace();

			}
	}

		@Override
		public Product searchProductId(int i) throws SQLException {
			Statement st = null;
			
			try {
				Connection conn = ConnectionDb.getConnection();
				String createString = "SELECT * FROM " + TABLENAME+" where productId= "+i;

				st=conn.createStatement();
				ResultSet res =st.executeQuery(createString); 


				while(res.next()== true) {

					String a =res.getString("name");
					int b = Integer.parseInt(res.getString("price"));
					int c = Integer.parseInt(res.getString("productId"));
					int d =Integer.parseInt(res.getString("quantity"));
					String e = res.getString("imageLink");

					product1.setName(a);
					product1.setPrice(b);
					product1.setProductId(c);
					product1.setQuantity(d);
					product1.setImageLink(e);

					
					
				}
				res.close();
			} catch (SQLException e) {
				System.out.println("ERROR");
				e.printStackTrace();

			}
			return product1;
		}
}
