package com.thinkopen.ecommercestruts.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.thinkopen.ecommercestruts.model.Orders;



public class OrderDaoImp implements OrderDao {
	
	private static final String TABLENAME="orders";
	List<Orders> list;
	Orders order1 = new Orders(0,0,0,0,null,0);
	
	public Boolean executeQuery(Connection conn, String command) throws SQLException {
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			stmt.executeQuery(command); 
			return true;
		} finally {

			if (stmt != null) { stmt.close(); }
		}
	}
	public boolean executeUpdate(Connection conn, String command) throws SQLException {
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(command); 
			return true;
		} finally {

			if (stmt != null) { stmt.close(); }
		}
	}
	@Override
	public void insertOrder(Orders o) throws SQLException {
		PreparedStatement ps = null;
		
		try {
			Connection conn = ConnectionDb.getConnection();
			String createString = "insert into "+ TABLENAME+ " values (null,?,?,?,?,?)";
			ps=conn.prepareStatement(createString);
			
			ps.setInt(1, o.getOrderId());
			ps.setInt(2, o.getCustomerId());
			ps.setInt(3, o.getProductId());
			ps.setString(4,o.getProductName());
			ps.setInt(5, o.getTotalPrice());
			ps.executeUpdate(); 

			System.out.println("insert ok");
			
		} catch (SQLException e) {
			System.out.println("insert error");
			e.printStackTrace();
			return;
			
		}
		

	}

	@Override
	public void deleteOrder(int m) throws SQLException {
		Statement st = null;
		try {
			Connection conn = ConnectionDb.getConnection();
			String createString = "delete from "+ TABLENAME+ " where orderNumber= "+m;
			
			st = conn.createStatement();
			st.executeUpdate(createString);
			System.out.println("delete ok");
			
		} catch (SQLException e) {
			
			System.out.println("delete error");
			e.printStackTrace();
		}
		
	}

	@Override
	public List<Orders> searchOrder(int l) throws SQLException {
		Statement st = null;
		list=new ArrayList<Orders>();
		try {
			Connection conn = ConnectionDb.getConnection();
			String createString = "SELECT * FROM " + TABLENAME+ " where orderId = "+l;

			st=conn.createStatement();
			ResultSet res =st.executeQuery(createString); 


			while(res.next()== true) {
				int f = Integer.parseInt(res.getString("orderNumber"));
				int a =Integer.parseInt(res.getString("orderId"));
				int b = Integer.parseInt(res.getString("customerId"));
				int c = Integer.parseInt(res.getString("productId"));
				int d =Integer.parseInt(res.getString("totalPrice"));
				String e= res.getString("productName");

				order1.setOrderNumber(f);
				order1.setOrderId(a);
				order1.setCustomerId(b);
				order1.setProductId(c);
				order1.setTotalPrice(d);
				order1.setProductName(e);

				list.add(new Orders(order1.getOrderNumber(),order1.getOrderId(),order1.getCustomerId(),order1.getProductId(),order1.getProductName(),order1.getTotalPrice()));
				
			}
			
			res.close();
		} catch (SQLException e) {
			System.out.println("ERROR");
			e.printStackTrace();

		}
		return list;
		
		
		
		

	}

	

}
