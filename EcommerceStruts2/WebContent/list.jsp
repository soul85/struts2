<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ include file="init.jsp" %>
<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>List</title>
	<style type="text/css">
		.img-rounded {
 	 		border-radius: 8px;
		}
		
	</style>
	
</head>
<body>
	<div id="wrap">

		
		<nav class="navbar navbar-default" role="navigation">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-ex1-collapse">
					<span class="sr-only"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span> <span class="icon-bar"></span>
				</button>

			</div>

			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="searchOrd">Basket <span class="glyphicon glyphicon-shopping-cart" ></span> </a></li>
					<li><a href="welcome.jsp">Logout <span class="glyphicon glyphicon-off" ></span> </a></li>
					<li><a href="getHistory">Order History </a></li>
				</ul>
				<p class="navbar-text"><s:property value="#session.LoginSession.getName()" />
					<s:property value="#session.LoginSession.getSurname()" /> <span class="glyphicon glyphicon-user" ></span> </p>
			</div>
		</nav>

		
		
		<div class="container" >
			<div class="row">
				<div class ="col-md-4">
					<h2>List of Products </h2>
				</div>
			</div>
		</div>
		
		<br>


		
		<s:iterator value="listProd" status="row" >
			
		<s:if test= "quantity == 0"> 
		<div class="container" style ="background-color: #E4E5E0;">
			<div class="row">
				<div class="col-md-4">
					<h2>
						 <s:property value="name" />
					</h2>
					<img alt="<s:property value="name" />" src="<s:property value="imageLink" />" width=auto height="400" class="img-rounded" >
					price:
					<s:property value="price" />
					euro
				</div>
			</div>
			
			<s:form action="outofstock.html">
				<s:hidden   value ="%{productId}" name="productIdProd"/>
				<s:submit value="add to basket" class="btn btn-primary btn-lg"/>
				</s:form>
				<span class="glyphicon glyphicon-shopping-cart" ></span><br>
				<p class="text-danger"> product out of stock </p>
				
			
		</div>
		<br>
		<br>
		
		</s:if>
		<s:else>

	
		<div class="container" style ="background-color: #E4E5E0;">
			<div class="row">
				<div class="col-md-4">
					<h2>
						<s:property value="name" />
					</h2>
					<img alt="<s:property value="name" />" src="<s:property value="imageLink" />" width=auto height="400" class="img-rounded">
					price:
					<s:property value="price" />
					euro
				</div>
			</div>
			<s:form action="insertOrder">
				<s:hidden   value ="%{productId}" name="productIdProd"/>
				<s:submit value="add to basket" class="btn btn-primary btn-lg"/>
				</s:form>
				<p class="text-success"> product in stock: 
		 			<s:property value="quantity" />
		 			
				Pz available </p>
			
			
	
		</div>
		<br>
		<br> 
		</s:else>
	</s:iterator>
	</div> 
			<div id="footer">
				<div class="container">
					<ul class="nav nav-tabs">
					<li class="active"><a href="mailto:shop@email.com">Contact</a></li>
					<li class="active"> <a href="">FAQ</a></li>
					<li class ="active"> <a href="">CONDITIONS OF USE</a> </li>
					</ul>
					
				</div>
				<div class="container" style ="background-color: #CADABA; >
			
					<div class="row" >
						<div class="col-md-4 col-md-offset-4" " >
						<p class ="text-muted credit">Copyright 2015 ThinkOpen srl PI 123456785</p>
					</div>
				</div>
			

	</body>



</html>