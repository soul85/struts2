<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ include file="init.jsp" %>
<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>Recap</title>

</head>
<body>
	<div id="wrap">
		
		<nav class="navbar navbar-default" role="navigation">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-ex1-collapse">
					<span class="sr-only"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span> <span class="icon-bar"></span>
				</button>

			</div>

			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="getProducts">Shop</a></li>
					<li><a href="searchOrd">Basket <span class="glyphicon glyphicon-shopping-cart" ></span> </a></li>
					<li><a href="welcome.jsp">Logout <span class="glyphicon glyphicon-off" ></span> </a></li>

				</ul>
				<p class="navbar-text"><s:property value="#session.LoginSession.getName()" />
					<s:property value="#session.LoginSession.getSurname()" /> <span class="glyphicon glyphicon-user" ></span> </p>
			</div>
		</nav>
		<br>
		<br>
	<div style ="background-color: #E4E5E0;">
			<h2>Order details</h2>
			<br>
		
			<table class="table">
				<thead>
					<tr>
						
						<td>Order Id</td>
						<td>Customer Id</td>
						<td>Date</td>
						<td>Total Price</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						
						<td><s:property value="#session.rand"/></td>
						<td><s:property value="#session.LoginSession.getCustomerId()"/></td>
						<td><s:property value="dateString" /></td>
						<td><s:property value="tot" /></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-12">
			
					
					<s:form action="insertRecap">
						<s:hidden value="%{tot}" name="total"/>
						<s:hidden value="%{dateString}" name="date"/>
						<s:submit value="pay" class="btn btn-primary btn-lg"/>
					</s:form>
				</div>
			</div>
		</div>



	</div> 
			<div id="footer">
				<div class="container">
					<ul class="nav nav-tabs">
					<li class="active"><a href="mailto:shop@email.com">Contact</a></li>
					<li class="active"> <a href="">FAQ</a></li>
					<li class ="active"> <a href="">CONDITIONS OF USE</a> </li>
					</ul>
					
				</div>
				<div class="container" style ="background-color: #CADABA;" >
			
					<div class="row" >
						<div class="col-md-4 col-md-offset-4"  >
						<p class ="text-muted credit">Copyright 2015 ThinkOpen srl PI 123456785</p>
					</div>
				</div>
			</div>
		</div>

	</body>
</html>