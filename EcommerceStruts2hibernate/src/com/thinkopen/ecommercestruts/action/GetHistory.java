package com.thinkopen.ecommercestruts.action;


import java.util.List;

import org.apache.struts2.dispatcher.SessionMap;
import org.hibernate.HibernateException;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.thinkopen.ecommercestruts.database.RecapDao;
import com.thinkopen.ecommercestruts.database.RecapDaoImp;
import com.thinkopen.ecommercestruts.model.Customer;
import com.thinkopen.ecommercestruts.model.RecapOrder;

public class GetHistory extends ActionSupport{

	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("unchecked")
	SessionMap<String, Object> session = (SessionMap<String, Object>) ActionContext.getContext().get("session");
	
	RecapDao recapDao = new RecapDaoImp();
	private List<RecapOrder> listHistory = null;
	private int customerId=0;
	
	public List<RecapOrder> getListHistory() {
		return listHistory;
	}
	public void setListHistory(List<RecapOrder> listHistory) {
		this.listHistory = listHistory;
	}
	public int getId() {
		
		try {
			 customerId =((Customer) session.get("LoginSession")).getCustomerId();
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
		
		return customerId;
	}
	
	public String execute(){
		
		try {
			listHistory=recapDao.searchRecapCustomerId(this.getId());
			System.out.println(listHistory);
		} catch (HibernateException e) {
			
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
}
