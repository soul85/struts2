package com.thinkopen.ecommercestruts.action;


import java.io.StringWriter;


//import java.util.Properties;
//import java.io.StringWriter;
//import javax.mail.MessagingException;
//
//import org.apache.velocity.Template;
//import org.apache.velocity.VelocityContext;
//import org.apache.velocity.app.VelocityEngine;
//
//import model.MailUtility;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.servlet.ServletContext;

import org.apache.struts2.util.ServletContextAware;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.hibernate.HibernateException;

import com.opensymphony.xwork2.ActionSupport;
import com.thinkopen.ecommercestruts.database.CustomerDao;
import com.thinkopen.ecommercestruts.database.CustomerDaoImp;
import com.thinkopen.ecommercestruts.model.Customer;
import com.thinkopen.ecommercestruts.model.MailUtility;

public class Registration extends ActionSupport implements ServletContextAware {

	private static final long serialVersionUID = 1L;
	private Customer customer;
	private String name;
	private String surname;
	private String country;
	private String email;
	private String password;
	ServletContext servletContext;
	private String subject= null;
	private String text=null;
	
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext=servletContext;
		
	}
	
	public ServletContext getServletContext() {
		return servletContext;
	}
	public String execute(){
		
		customer=new Customer(0,name,surname,country,email,password);
		CustomerDao customerDao = new CustomerDaoImp();

		try {
			customerDao.insertCustomer(customer);
		} catch (HibernateException ex) {

			ex.printStackTrace();
		}
		
		Properties props = new Properties();
        props.setProperty("resource.loader", "webapp");
        props.setProperty("webapp.resource.loader.class", "org.apache.velocity.tools.view.WebappResourceLoader");

		if(this.getCountry().equalsIgnoreCase("England")){


			VelocityEngine ve = new VelocityEngine(props);
			ve.setApplicationAttribute("javax.servlet.ServletContext", this.getServletContext());
			ve.init();

			Template t = ve.getTemplate( "registrationEng.vm" );

			VelocityContext context = new VelocityContext();
			context.put("name", this.getName());
			context.put("pwd", this.getPassword());
			StringWriter writer = new StringWriter();
			t.merge( context, writer );
			subject="Registration to Shop Online";
			text = writer.toString();
			
		}else if (this.getCountry().equalsIgnoreCase("Italy")){

			VelocityEngine ve = new VelocityEngine(props);
			ve.setApplicationAttribute("javax.servlet.ServletContext", this.getServletContext());
			ve.init();

			Template t = ve.getTemplate( "registrationIta.vm" );

			VelocityContext context = new VelocityContext();
			context.put("name",  this.getName());
			context.put("pwd", this.getPassword());
			StringWriter writer = new StringWriter();
			t.merge( context, writer );
			subject="Registrazione al Negozio Online";
			text = writer.toString();


		}


		try {
			MailUtility.sendMail("paolopietro.oldrati@gmail.com",subject,text);
		} catch (MessagingException ex) {

			ex.printStackTrace();
		}
		return SUCCESS;
	}


	
}
