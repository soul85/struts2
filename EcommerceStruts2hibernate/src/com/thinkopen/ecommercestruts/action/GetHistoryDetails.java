package com.thinkopen.ecommercestruts.action;


import java.util.List;

import org.hibernate.HibernateException;

import com.opensymphony.xwork2.ActionSupport;
import com.thinkopen.ecommercestruts.database.OrderDao;
import com.thinkopen.ecommercestruts.database.OrderDaoImp;
import com.thinkopen.ecommercestruts.model.Orders;

public class GetHistoryDetails extends ActionSupport{

	private static final long serialVersionUID = 1L;
	private int orderId;
	private List<Orders> list=null;
	private int tot=0;
	
	
	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public List<Orders> getList() {
		return list;
	}

	public void setList(List<Orders> list) {
		this.list = list;
	}

	public int getTot() {
		return tot;
	}

	public void setTot(int tot) {
		this.tot = tot;
	}

	public String execute(){
		
		OrderDao orderDao=new OrderDaoImp();
		try {
			list=orderDao.searchOrder(orderId);
		} catch (HibernateException e) {
			
			e.printStackTrace();
		}
		
		for(Orders o:list){
			tot=tot+o.getTotalPrice();
		}
		
		return SUCCESS;
	}

}
