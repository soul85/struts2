package com.thinkopen.ecommercestruts.model;

import java.util.ArrayList;

public class Product {
	private String name;
	private int price;
	private int productId;
	private int quantity;
	private String imageLink;
	ArrayList<Product> listProducts = new ArrayList<Product>();
	
	
	public Product(){}
	public Product(String name, int price, int productId, int quantity,String imageLink) {
		this.name = name;
		this.price = price;
		this.productId = productId;
		this.quantity = quantity;
		this.imageLink= imageLink;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public String getImageLink() {
		return imageLink;
	}

	public void setImageLink(String imageLink) {
		this.imageLink = imageLink;
	}

	public boolean outOfStock(){
		if(this.getQuantity()==0){
			return true;
		} else{
			return false;
		}
	}


	public ArrayList<Product> getListProducts() {
		return listProducts;
	}

	public void setListProducts(ArrayList<Product> listProducts) {
		this.listProducts = listProducts;
	}

	@Override
	public String toString() {
		return "Product [name=" + name + ", price=" + price
				+ ", productId=" + productId + ", quantity=" + quantity
				+ "]";
	}

}
