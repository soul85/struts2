package com.thinkopen.ecommercestruts.database;


import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import com.thinkopen.ecommercestruts.model.Customer;

public class CustomerDaoImp implements CustomerDao {
	
	private boolean valid=true;
	
	@Override
	public void insertCustomer(Customer o) throws HibernateException {
		
		try{
		Session session=SessionFactoryUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(o);
        session.getTransaction().commit();
        session.close();
        
		
			System.out.println("insert ok");
			
		} catch (HibernateException e) {
			System.out.println("insert error");
			e.printStackTrace();
			return;
			
		}
		
	}

	@Override
	public Customer login(Customer o)throws HibernateException {
		Customer c= new Customer();
		try{
		Session session=SessionFactoryUtil.getSessionFactory().openSession();
		session.beginTransaction();
		Query query = session.createQuery("from Customer where email= :email and password = :password");
		query.setString("email", o.getEmail());
		query.setString("password", o.getPassword());
		c = (Customer) query.uniqueResult();
		System.out.println(c.getName());
		session.getTransaction().commit();
		
		session.close();
		System.out.println("login ok");
		
		
		
		}
		catch(NullPointerException ex){
			System.out.println("login error");
			valid=false;
		}
		
		return c;
	}
	
	@Override
	public boolean validLogin() {
		
		return valid;
	}

}
