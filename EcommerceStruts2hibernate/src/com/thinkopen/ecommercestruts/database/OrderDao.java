package com.thinkopen.ecommercestruts.database;


import java.util.List;

import org.hibernate.HibernateException;

import com.thinkopen.ecommercestruts.model.Orders;

public interface OrderDao {
		public void insertOrder(Orders o)throws HibernateException;
		public void deleteOrder(int m) throws HibernateException;
		public List<Orders> searchOrder(int l)throws HibernateException;
		
		
}
