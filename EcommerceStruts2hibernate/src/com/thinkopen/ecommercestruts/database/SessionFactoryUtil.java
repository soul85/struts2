package com.thinkopen.ecommercestruts.database;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;



public class SessionFactoryUtil {
private static SessionFactory sessionFactory;
	

	private static SessionFactory createSessionFactory(){
		
		 try
	      {
	         
	            Configuration configuration = new Configuration().configure(SessionFactoryUtil.class.getResource("/hibernate.cfg.xml"));
	            StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder();
	            serviceRegistryBuilder.applySettings(configuration.getProperties());
	            ServiceRegistry serviceRegistry = serviceRegistryBuilder.build();
	            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	            
	         
	         return sessionFactory;
	      } catch (Throwable ex)
	      {
	         System.err.println("Initial SessionFactory creation failed." + ex);
	         throw new ExceptionInInitializerError(ex);
	      }
		 
	   }
	
    public static SessionFactory getSessionFactory() {
    	if(sessionFactory == null) sessionFactory = createSessionFactory() ;
        return sessionFactory;
    }
    
    public static void shutdown()
    {
       getSessionFactory().close();
    }
}
