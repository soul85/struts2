package com.thinkopen.ecommercestruts.database;

import java.sql.SQLException;
import java.util.List;

import com.thinkopen.ecommercestruts.model.Product;

public interface ProductDao {
	public List<Product> searchProduct()throws SQLException;
	public Product searchProductId(int i)throws SQLException;
	public void  increaseQuantity(int i)throws SQLException;
	public void decreaseQuantity(int i) throws SQLException;
}
