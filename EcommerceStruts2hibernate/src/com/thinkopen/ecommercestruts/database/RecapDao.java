package com.thinkopen.ecommercestruts.database;


import java.util.List;

import org.hibernate.HibernateException;

import com.thinkopen.ecommercestruts.model.*;

public interface RecapDao {
	public void insertRecapOrder(RecapOrder o)throws HibernateException;
	public List<RecapOrder> searchRecapOrder(int i)throws HibernateException;
	public List<RecapOrder> searchRecapCustomerId(int i)throws HibernateException;
	
}
