package com.thinkopen.ecommercestruts.database;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.thinkopen.ecommercestruts.model.Orders;
import com.thinkopen.ecommercestruts.model.RecapOrder;

public class RecapDaoImp implements RecapDao {
	
	
	List<RecapOrder> list;
	
	List<RecapOrder> list2;
	
	
	@Override
	public void insertRecapOrder(RecapOrder o) throws HibernateException {
		
		try{
			Session session=SessionFactoryUtil.getSessionFactory().openSession();
	        session.beginTransaction();
	        session.save(o);
	        session.getTransaction().commit();
	        
	        session.close();
	        
			
				System.out.println("insert ok");
				
			} catch (HibernateException e) {
				System.out.println("insert error");
				e.printStackTrace();
				return;
				
			}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RecapOrder> searchRecapOrder(int i) throws HibernateException {
		
		list=new ArrayList<RecapOrder>();
		
		try {
			Session session=SessionFactoryUtil.getSessionFactory().openSession();
	        session.beginTransaction();
	        Criteria cr = session.createCriteria(Orders.class);
	        cr.add(Restrictions.eq("orderId", i));
	        list=cr.list();
//	        Query query = session.createQuery("from RecapOrder where orderId=:id");
//	        query.setInteger("id", i);
//	        list=query.list();
	        session.getTransaction().commit();
			session.close();
			
			
		} catch (HibernateException e) {
			e.printStackTrace();
		}

		return list;
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RecapOrder> searchRecapCustomerId(int i) throws HibernateException {
		
		list2=new ArrayList<RecapOrder>();
		
		try {
			Session session=SessionFactoryUtil.getSessionFactory().openSession();
	        session.beginTransaction();
	        Query query = session.createQuery("from RecapOrder where customerId=:id");
	        query.setInteger("id", i);
	        list2=query.list();
	        session.getTransaction().commit();
			session.close();
			
			
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		
		return list2;
		
		
	}

}
