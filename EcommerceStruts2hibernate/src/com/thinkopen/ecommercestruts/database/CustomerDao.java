package com.thinkopen.ecommercestruts.database;


import org.hibernate.HibernateException;

import com.thinkopen.ecommercestruts.model.Customer;

public interface CustomerDao {
		public void insertCustomer(Customer o)throws HibernateException;
		public Customer login(Customer o)throws HibernateException;
		public boolean validLogin();
}
