package com.thinkopen.ecommercestruts.database;




import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import com.thinkopen.ecommercestruts.model.Product;

public class ProductDaoImp implements ProductDao {
	
	private static final String TABLENAME="products";
	List<Product> list;
	Product product1= new Product(null,0,0,0,null);
	
	public boolean executeUpdate(Connection conn, String command) throws SQLException {
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(command); 
			return true;
		} finally {

			if (stmt != null) { stmt.close(); }
		}
	}

	public Boolean executeQuery(Connection conn, String command) throws SQLException {
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			stmt.executeQuery(command); 
			return true;
		} finally {

			if (stmt != null) { stmt.close(); }
		}
	}
	
	
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Product> searchProduct() throws HibernateException {
		
		list=new ArrayList<Product>();
		
		try {
			Session session=SessionFactoryUtil.getSessionFactory().openSession();
	        session.beginTransaction();
	        Query query = session.createQuery("from Product");
	        list=query.list();
	        session.getTransaction().commit();
			session.close();
			
			
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		

		return list;
	}
	@Override
	public void increaseQuantity(int i) throws SQLException {
		
		Statement st = null;
		try {
			Connection conn = ConnectionDb.getConnection();
			String createString = "update "+ TABLENAME+ " set quantity=quantity+1 where productId= "+i;

			st=conn.createStatement();
			st.executeUpdate(createString);
			System.out.println("increase ok");
			
		}catch (SQLException e) {
			
			System.out.println("increase error");
			e.printStackTrace();

		}
//		try {
//			Session session=SessionFactoryUtil.getSessionFactory().openSession();
//	        session.beginTransaction();
//	        Query query = session.createQuery("update Product set quantity=quantity + 1 where productId= :id");
////	        query.setInteger("increment", 1);
//	        query.setInteger("id", i);
//	        
//	        
//	        session.getTransaction().commit();
//			session.close();
//			
//			
//		} catch (HibernateException e) {
//			e.printStackTrace();
//		}

		
	}
		
		public void decreaseQuantity(int i) throws SQLException {
			
			Statement st = null;
			try {
				Connection conn = ConnectionDb.getConnection();
				String createString = "update "+ TABLENAME+ " set quantity=quantity-1 where productId= "+i;

				st=conn.createStatement();
				st.executeUpdate(createString);
				System.out.println("decrease ok");

			}catch (SQLException e) {
				System.out.println("decrease error");
				e.printStackTrace();

			}
			
//			try {
//				Session session=SessionFactoryUtil.getSessionFactory().openSession();
//		        session.beginTransaction();
//		        Query query = session.createQuery("update Product set quantity=quantity - 1 where productId= :id");
////		        query.setInteger("decrease", 1);
//		        query.setInteger("id", i);
//		        
//		        
//		        session.getTransaction().commit();
//				session.close();
//				
//				
//			} catch (HibernateException e) {
//				e.printStackTrace();
//			}

	}

		@Override
		public Product searchProductId(int i) throws HibernateException {
			Product product1= new Product();
			try {
				Session session=SessionFactoryUtil.getSessionFactory().openSession();
				session.beginTransaction();
				Query query = session.createQuery("from Product where productId= :id");
				query.setInteger("id", i);
				product1=(Product) query.uniqueResult();
				session.getTransaction().commit();
				session.close();
				
			} catch (HibernateException e) {
				System.out.println("error");
				e.printStackTrace();
			}

			return product1;
		}
}
