package com.thinkopen.ecommercestruts.database;



import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.thinkopen.ecommercestruts.model.Orders;



public class OrderDaoImp implements OrderDao {
	List<Orders> list;
	
	@Override
	public void insertOrder(Orders o) throws HibernateException {
		
		try{
			Session session=SessionFactoryUtil.getSessionFactory().openSession();
	        session.beginTransaction();
	        session.save(o);
	        session.getTransaction().commit();
	        session.close();
	        
			
				System.out.println("insert ok");
				
			} catch (HibernateException e) {
				System.out.println("insert error");
				e.printStackTrace();
				return;
				
			}
		
		

		

	}

	@Override
	public void deleteOrder(int m) throws HibernateException {
//		Orders order=new Orders();
		try{
			Session session=SessionFactoryUtil.getSessionFactory().openSession();
	        session.beginTransaction();
//	        order.setProductId(m);
//	        order.setOrderId(n);
//	        session.delete(order);
	        Query query = session.createQuery("delete Orders where orderNumber= :id ");
	        query.setInteger("id", m);
	        
	        
	        query.executeUpdate();
	        session.getTransaction().commit();
	        session.close();
	        
			
				System.out.println("delete ok");
				
			} catch (HibernateException e) {
				System.out.println("delete error");
				e.printStackTrace();
				return;
				
			}
		

		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Orders> searchOrder(int l) throws HibernateException {
		
		list=new ArrayList<Orders>();
		
		try {
			Session session=SessionFactoryUtil.getSessionFactory().openSession();
	        session.beginTransaction();
	        Criteria cr = session.createCriteria(Orders.class);
	        cr.add(Restrictions.eq("orderId", l));
	        list=cr.list();
//	        Query query = session.createQuery("from Orders where orderId=:id");
//	        query.setInteger("id", l);
//	        list=query.list();
	        session.getTransaction().commit();
			session.close();
			
			
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		

		return list;
	

	}

	
}
